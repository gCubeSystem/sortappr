# Dockerfile
#
# version: 1.0
# author: Giancarlo Panichi
# date: 2020/10/09

# R images 
# see for more image:
# https://hub.docker.com/u/rocker/
#

# Version-stable build of R, rstudio, and R packages ~2.48GB
# FROM rocker/tidyverse:latest

#
# In this case I will use the r-base image by adding some debian packages 
# necessary for the correct build of the image.
# So, I will obtain an image of ~1.2GB.
# Better ad-hoc solutions can be found to reduce the image size.
#
# Base Version ~824MB
FROM rocker/r-base:latest

RUN apt-get update -qq && apt-get -y --no-install-recommends install \
  libssl-dev \  
  libxml2-dev \
  libcairo2-dev \
  libssh2-1-dev \
  libsasl2-dev \
  libcurl4-openssl-dev 


## create directories
RUN mkdir -p /code
WORKDIR /code
RUN mkdir -p src

## copy files
COPY /src/* src/
COPY sortapp .
RUN chmod +x sortapp

## install R-packages
RUN Rscript src/install_packages.R


## create a link
WORKDIR /
RUN ln -sf /code/sortapp /usr/bin/sortapp

# RUN Script
# CMD sortapp
