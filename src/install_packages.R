# 
# Install Packages
#
# version: 1.0
# author: Giancarlo Panichi
# date: 2020/10/09
#
# Uncomment and add necessary packages
#
#install.packages("devtools", repos = "http://cran.rstudio.com/",dependencies=TRUE)
#install.packages("dplyr", dependencies=TRUE)
#install.packages("ggplot2", dependencies=TRUE)
#install.packages("forcats", dependencies=TRUE)
install.packages("XML", dependencies=TRUE)
install.packages("jsonline", dependencies=TRUE)
install.packages("stringr", dependencies=TRUE)
install.packages("httr", dependencies=TRUE)

