# SortAppR

SortAppR is a simple example that allows you to sort a file in input by R.
Starting from this example, you can first create an simple script R and then a docker image in which it is installed the created script.
This example declare the sortapp command as entrypoint.
So once you can use this command at command line to run the example:

```
sortapp <token> <file-item-id> <temp-dir-item-id> 
```

The image created in this way can be executed in a container with the following command:

```
docker run -i -t --rm --name sortappr-cont sortappr sortapp <token> <file-item-id> <temp-dir-item-id>
``` 

To be able to create an image from this application you need to have Docker and Docker-Compose installed on your machine and the relative python packages, see: 
[Docker](https://docs.docker.com/engine/),
[Docker-Compose](https://docs.docker.com/compose/install/) and
[Docker Package for Python](https://pypi.org/project/docker/).

## Useful Commands

### Create Docker Image 
```
docker build -t sortappr .
```

### Save Docker Image in file
```
docker save sortappr | gzip > sortappr.tar.gz
```

### Publish Docker Image on DockerHub
Re-tagging an existing local image: 

```
docker tag sortappr <hub-user>/<repo-name>[:<tag>]
```

Login in DockerHub(use your Docker ID):

```
docker login
```

Now you can push this repository to the registry designated by its name or tag:

```
docker push <hub-user>/<repo-name>:<tag>
```

Then logout for security:

```
docker logout
```

## Authors

* **Giancarlo Panichi** ([ORCID](http://orcid.org/0000-0001-8375-6644)) - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)



## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

